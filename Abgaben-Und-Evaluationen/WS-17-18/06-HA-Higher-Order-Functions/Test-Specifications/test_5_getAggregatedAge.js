describe("Aufgabe 6, Teilaufgabe 5, getAggregatedAge: ", function () {
    it('should be able to get the aggregated age of the example array from the task description', function () {
        var persArray = [
            ['Max', 1967, 2, 'm'],
            ['Maria', 1990, 1, 'f'],
            ['Meik', 1982, 2, 'm'],
            ['Mareike', 1985, 3, 'f'],
            ['Mervin', 1971, 1, 'm'],
            ['Magdalena', 1979, 1, 'f'],
        ];

        var result = 228;

        expect(getAggregatedAge(persArray)).toEqual(result);
    });
    
    it('should be able to get the aggregated age of very old persons', function () {
        var persArray = [
            ['Max', 1902, 2, 'm'],
            ['Maria', 1998, 1, 'f'],
            ['Meik', 1978, 2, 'm'],
            ['Mareike', 1909, 3, 'f'],
            ['Mervin', 1546, 1, 'm'],
            ['Magdalena', 1777, 1, 'f'],
            ['Hermann', 1988, 2, 'm'],
            ['Lars', 1999, 1, 'h'],
            ['Frauke', 2005, 2, 's'],
            ['Katja', 2000, 2, 'f'],
            ['Anika', 1385, 3, 'f'],
            ['Hans', 1945, 1, 'f'],
            ['Knut', 1950, 1, 'f']
        ];

        var result = 1839;

        expect(getAggregatedAge(persArray)).toEqual(result);
    });
    
    it('should be able to get the aggregated age of only one persons', function () {
        var persArray = [
            ['Anika', 1385, 3, 'f']
        ];

        var result = 632;

        expect(getAggregatedAge(persArray)).toEqual(result);
    });



    it('Higher-Order-Function should be used', function () {
        var persArray = [
            ['Max', 1967, 2, 'm'],
            ['Maria', 1990, 1, 'f'],
            ['Meik', 1982, 2, 'm'],
            ['Maik', 1982, 2, 'm'],
            ['Mareike', 1985, 3, 'f'],
            ['Mervin', 1971, 1, 'm'],
            ['Magdalena', 1979, 1, 'f'],
        ];

        HOFSpyWrapper.registerHOFSpies(persArray);
        getAggregatedAge(persArray);


        expect(HOFSpyWrapper.hofUsed(persArray)).toBe(true);
    });
});
