describe("Aufgabe 6, Teilaufgabe 1, filterGender: ", function () {
    it('should be able to filter the example array from the task description', function () {
        var persArray = [
            ['Max', 1967, 2, 'm'],
            ['Maria', 1990, 1, 'f'],
            ['Meik', 1982, 2, 'm'],
            ['Mareike', 1985, 3, 'f'],
            ['Mervin', 1971, 1, 'm'],
            ['Magdalena', 1979, 1, 'f'],
        ];

        var result = [
            ['Maria', 1990, 1, 'f'],
            ['Mareike', 1985, 3, 'f'],
            ['Magdalena', 1979, 1, 'f']
        ];

        expect(filterGender(persArray, 'f')).toEqual(result);
    });

    it('should be able to filter a long array', function () {
        var persArray = [
            ['Max', 1967, 2, 'm'],
            ['Maria', 1990, 1, 'f'],
            ['Meik', 1982, 2, 'm'],
            ['Maik', 1982, 2, 'm'],
            ['Mareike', 1985, 3, 'f'],
            ['Mervin', 1971, 1, 'm'],
            ['Magdalena', 1979, 1, 'f'],
            ['Hermann', 1967, 2, 'm'],
            ['Lars', 1990, 1, 'h'],
            ['Frauke', 1982, 2, 's'],
            ['Katja', 1982, 2, 'f'],
            ['Anika', 1985, 3, 'f'],
            ['Hans', 1971, 1, 'f'],
            ['Knut', 1979, 1, 'f']
        ];
        
        var result = [
            ['Max', 1967, 2, 'm'],
            ['Meik', 1982, 2, 'm'],
            ['Maik', 1982, 2, 'm'],
            ['Mervin', 1971, 1, 'm'],
            ['Hermann', 1967, 2, 'm']
        ];

        expect(filterGender(persArray, 'm')).toEqual(result);
    });

    it('should be able to filter an array with only one female', function () {
        var persArray = [
            ['Berta', 1990, 1, 'f'],
        ];

        var result = [
            ['Berta', 1990, 1, 'f']
        ];

        expect(filterGender(persArray, 'f')).toEqual(result);
    });

    
//     it('Genau eine weibliche Person, filtere männliche Personen', function () {
//         var persArray = [
//             ['Maria', 1990, 1, 'f'],
//         ];
// 
//         var result = [];
// 
//         expect(filterGender(persArray, 'm')).toEqual(result);
//     });

    it('it should be able to filter an array with multiple identical persons', function () {
        var persArray = [
            ['Max', 1967, 2, 'm'],
            ['Maria', 1990, 1, 'f'],
            ['Meik', 1982, 2, 'm'],
            ['Maik', 1982, 2, 'm'],
            ['Mareike', 1985, 3, 'f'],
            ['Mervin', 1971, 1, 'm'],
            ['Magdalena', 1979, 1, 'f'],
            ['Max', 1967, 2, 'm'],
            ['Maria', 1990, 1, 'f'],
            ['Meik', 1982, 2, 'm'],
            ['Maik', 1982, 2, 'm'],
            ['Mareike', 1985, 3, 'f'],
            ['Mervin', 1971, 1, 'm'],
            ['Magdalena', 1979, 1, 'f']
        ];
        
        var result = [
            ['Max', 1967, 2, 'm'],
            ['Meik', 1982, 2, 'm'],
            ['Maik', 1982, 2, 'm'],
            ['Mervin', 1971, 1, 'm'],
            ['Max', 1967, 2, 'm'],
            ['Meik', 1982, 2, 'm'],
            ['Maik', 1982, 2, 'm'],
            ['Mervin', 1971, 1, 'm']
        ];

        expect(filterGender(persArray, 'm')).toEqual(result);
    });

    it('Higher-Order-Function should be used', function () {
        var persArray = [
            ['Max', 1967, 2, 'm'],
            ['Maria', 1990, 1, 'f'],
            ['Meik', 1982, 2, 'm'],
            ['Maik', 1982, 2, 'm'],
            ['Mareike', 1985, 3, 'f'],
            ['Mervin', 1971, 1, 'm'],
            ['Magdalena', 1979, 1, 'f'],
        ];

        HOFSpyWrapper.registerHOFSpies(persArray);
        filterGender(persArray, 'f');


        expect(HOFSpyWrapper.hofUsed(persArray)).toBe(true);
    });
});
