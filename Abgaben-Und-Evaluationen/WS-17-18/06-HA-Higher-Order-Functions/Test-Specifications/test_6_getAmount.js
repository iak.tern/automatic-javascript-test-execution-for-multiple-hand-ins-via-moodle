describe("Aufgabe 6, Teilaufgabe 6, getAmount: ", function () {
    it('should be able to get the amount of the example array from the task description', function () {
        var persArray = [
            ['Max', 1967, 2, 'm'],
            ['Maria', 1990, 1, 'f'],
            ['Meik', 1982, 2, 'm'],
            ['Mareike', 1985, 3, 'f'],
            ['Mervin', 1971, 1, 'm'],
            ['Magdalena', 1979, 1, 'f']
        ];

        var result = {Woman: 3, Women: 3, Men: 3, Man: 3};
        expect( result ).toEqual( jasmine.objectContaining( getAmount(persArray) ) );
    });

    it('should be able to get the amount of a long array', function () {
        var persArray = [
            ['Max', 1902, 2, 'm'],
            ['Maria', 1998, 1, 'f'],
            ['Meik', 1978, 2, 'm'],
            ['Mareike', 1909, 3, 'f'],
            ['Mervin', 1546, 1, 'm'],
            ['Magdalena', 1777, 1, 'f'],
            ['Hermann', 1988, 2, 'm'],
            ['Lars', 1999, 1, 'm'],
            ['Frauke', 2005, 2, 'f'],
            ['Katja', 2000, 2, 'f'],
            ['Anika', 1385, 3, 'f'],
            ['Hans', 1945, 1, 'f'],
            ['Knut', 1950, 1, 'f']
        ];

        var result = {Woman: 8, Women: 8, Men: 5, Man: 5};
        expect( result ).toEqual( jasmine.objectContaining( getAmount(persArray) ) );
    });
    
    it('should be able to get the amount of an array without mens', function () {
        var persArray = [
            ['Frauke', 2005, 2, 'f'],
            ['Katja', 2000, 2, 'f'],
            ['Anika', 1385, 3, 'f'],
            ['Hans', 1945, 1, 'f'],
            ['Knut', 1950, 1, 'f']
        ];

        var result = {Woman: 5, Women: 5, Men: 0, Man: 0};
        expect( result ).toEqual( jasmine.objectContaining( getAmount(persArray) ) );
    });
    
/*    it('should be able to get the amount of an array with undefined gender', function () {
        var persArray = [
            ['Max', 1967, 2, 'm'],
            ['Maria', 1990, 1, 'f'],
            ['Meik', 1982, 2, 's'],
            ['Mareike', 1985, 3, 'h'],
            ['Mervin', 1971, 1, 'r'],
            ['Magdalena', 1979, 1, 'f']
        ];

        var result = {Woman: 2, Women: 2, Men: 1, Man: 1};
        expect( result ).toEqual( jasmine.objectContaining( getAmount(persArray) ) );
    });*/


    it('Higher-Order-Function should be used', function () {
        var persArray = [
            ['Max', 1967, 2, 'm'],
            ['Maria', 1990, 1, 'f'],
            ['Meik', 1982, 2, 'm'],
            ['Maik', 1982, 2, 'm'],
            ['Mareike', 1985, 3, 'f'],
            ['Mervin', 1971, 1, 'm'],
            ['Magdalena', 1979, 1, 'f'],
        ];

        HOFSpyWrapper.registerHOFSpies(persArray);
        getAmount(persArray);


        expect(HOFSpyWrapper.hofUsed(persArray)).toBe(true);
    });
});
