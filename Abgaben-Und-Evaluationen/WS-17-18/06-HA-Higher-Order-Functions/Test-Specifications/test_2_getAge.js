describe("Aufgabe 6, Teilaufgabe 2, getAge: ", function () {
    it('should be able to calculate the ages of example array from the task description', function () {
        var persArray = [
            ['Max', 1967, 2, 'm'],
            ['Maria', 1990, 1, 'f'],
            ['Meik', 1982, 2, 'm'],
            ['Mareike', 1985, 3, 'f'],
            ['Mervin', 1971, 1, 'm'],
            ['Magdalena', 1979, 1, 'f'],
        ];

        var result = [
            ['Max', 50],
            ['Maria', 27],
            ['Meik', 35],
            ['Mareike', 32],
            ['Mervin', 46],
            ['Magdalena', 38]
        ];

        expect(getAge(persArray)).toEqual(result);
    });

     it('should be able to calculate the ages of a long array', function () {
        var persArray = [
            ['Max', 1902, 2, 'm'],
            ['Maria', 1998, 1, 'f'],
            ['Meik', 1978, 2, 'm'],
            ['Maik', 1409, 2, 'm'],
            ['Mareike', 1909, 3, 'f'],
            ['Mervin', 1546, 1, 'm'],
            ['Magdalena', 1777, 1, 'f'],
            ['Hermann', 1988, 2, 'm'],
            ['Lars', 1999, 1, 'h'],
            ['Frauke', 2005, 2, 's'],
            ['Katja', 2000, 2, 'f'],
            ['Anika', 1385, 3, 'f'],
            ['Hans', 1945, 1, 'f'],
            ['Knut', 1950, 1, 'f']
        ];
        
        var result = [
            [ 'Max', 115 ],
            [ 'Maria', 19 ],
            [ 'Meik', 39 ],
            [ 'Maik', 608 ],
            [ 'Mareike', 108 ],
            [ 'Mervin', 471 ],
            [ 'Magdalena', 240 ],
            [ 'Hermann', 29 ],
            [ 'Lars', 18 ],
            [ 'Frauke', 12 ],
            [ 'Katja', 17 ],
            [ 'Anika', 632 ],
            [ 'Hans', 72 ],
            [ 'Knut', 67 ] 
        ];

        expect(getAge(persArray, 'm')).toEqual(result);
    });

    it('Higher-Order-Function should be used', function () {
        var persArray = [
            ['Max', 1967, 2, 'm'],
            ['Maria', 1990, 1, 'f'],
            ['Meik', 1982, 2, 'm'],
            ['Maik', 1982, 2, 'm'],
            ['Mareike', 1985, 3, 'f'],
            ['Mervin', 1971, 1, 'm'],
            ['Magdalena', 1979, 1, 'f'],
        ];

        HOFSpyWrapper.registerHOFSpies(persArray);
        getAge(persArray);


        expect(HOFSpyWrapper.hofUsed(persArray)).toBe(true);
    });
});
