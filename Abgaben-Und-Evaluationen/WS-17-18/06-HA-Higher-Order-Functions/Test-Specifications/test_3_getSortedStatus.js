describe("Aufgabe 6, Teilaufgabe 3, getSortedStatus: ", function () {
    it('should be able to sort the example array from the task description', function () {
        var persArray = [
            ['Max', 1967, 2, 'm'],
            ['Maria', 1990, 1, 'f'],
            ['Meik', 1982, 2, 'm'],
            ['Mareike', 1985, 3, 'f'],
            ['Mervin', 1971, 1, 'm'],
            ['Magdalena', 1979, 1, 'f'],
        ];

        var result = [
            ['Max', 100],
            ['Mareike', 96],
            ['Meik', 70],
            ['Mervin', 46],
            ['Magdalena', 38],
            ['Maria', 27]
        ];


        expect(getSortedStatus(persArray)).toEqual(result);
    });
    
    it('should be able to sort a long array with very old persons', function () {
        var persArray = [
            ['Max', 1902, 2, 'm'],
            ['Maria', 1998, 1, 'f'],
            ['Meik', 1978, 2, 'm'],
            ['Maik', 1409, 2, 'm'],
            ['Mareike', 1909, 3, 'f'],
            ['Mervin', 1546, 1, 'm'],
            ['Magdalena', 1777, 1, 'f'],
            ['Hermann', 1988, 2, 'm'],
            ['Lars', 1999, 1, 'h'],
            ['Frauke', 2005, 2, 's'],
            ['Katja', 2000, 2, 'f'],
            ['Anika', 1385, 3, 'f'],
            ['Hans', 1945, 1, 'f'],
            ['Knut', 1950, 1, 'f']
        ];

        var result = [
            [ 'Anika', 1896 ],
            [ 'Maik', 1216 ],
            [ 'Mervin', 471 ],
            [ 'Mareike', 324 ],
            [ 'Magdalena', 240 ],
            [ 'Max', 230 ],
            [ 'Meik', 78 ],
            [ 'Hans', 72 ],
            [ 'Knut', 67 ],
            [ 'Hermann', 58 ],
            [ 'Katja', 34 ],
            [ 'Frauke', 24 ],
            [ 'Maria', 19 ],
            [ 'Lars', 18 ] 
        ];


        expect(getSortedStatus(persArray)).toEqual(result);
    });
    
    it('should be able to sort an array with one person', function () {
        var persArray = [
            ['Mareike', 1985, 3, 'f']
        ];

        var result = [
            ['Mareike', 96]
        ];


        expect(getSortedStatus(persArray)).toEqual(result);
    });
    
    it('should be able to sort an array with status values of 0, 15 and 300', function () {
        var persArray = [
            ['Max', 1967, 0, 'm'],
            ['Maria', 1990, 300, 'f'],
            ['Meik', 1982, 2, 'm'],
            ['Mareike', 1985, 3, 'f'],
            ['Mervin', 1971, 15, 'm'],
            ['Magdalena', 1979, 1, 'f'],
        ];

        var result = [
            ['Maria', 8100], 
            ['Mervin', 690],
            ['Mareike', 96],
            ['Meik', 70],
            ['Magdalena', 38],
            ['Max', 0]
        ];


        expect(getSortedStatus(persArray)).toEqual(result);
    });
    
    it('should be able to sort an array the same status after the name', function () {
        var persArray = [
            ['Mak', 1990, 1, 'm'],
            ['Mox', 1990, 1, 'm'],
            ['Maria', 1990, 1, 'f'],
            ['Meik', 1990, 1, 'm'],
            ['Muk', 1990, 1, 'm']
        ];

        var result = [
            ['Mak', 27],
            ['Maria', 27],
            ['Meik', 27],
            ['Mox', 27],
            ['Muk', 27]
        ];


        expect(getSortedStatus(persArray)).toEqual(result);
    });



    it('Higher-Order-Function should be used', function () {
        var persArray = [
            ['Max', 1967, 2, 'm'],
            ['Maria', 1990, 1, 'f'],
            ['Meik', 1982, 2, 'm'],
            ['Maik', 1982, 2, 'm'],
            ['Mareike', 1985, 3, 'f'],
            ['Mervin', 1971, 1, 'm'],
            ['Magdalena', 1979, 1, 'f'],
        ];

        HOFSpyWrapper.registerHOFSpies(persArray);
        getSortedStatus(persArray);


        expect(HOFSpyWrapper.hofUsed(persArray)).toBe(true);
        // expect(persArray.map).toHaveBeenCalled()
        // expect(persArray.sort).toHaveBeenCalled()

    });
});
