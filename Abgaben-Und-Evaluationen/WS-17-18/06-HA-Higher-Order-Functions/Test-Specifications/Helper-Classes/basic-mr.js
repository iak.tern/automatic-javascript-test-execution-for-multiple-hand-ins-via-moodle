// Dies ist ein beispielhaftes Personen-Array, welches den Aufbau verdeutlicht
// Es enthält weitere Arrays mit [name, birthYear, status, gender].
const personsHelper = [
    ['Max', 1967, 2, 'm'],
    ['Maria', 1990, 1, 'f'],
    ['Meik', 1982, 2, 'm'],
    ['Maik', 1982, 2, 'm'],
    ['Mareike', 1985, 3, 'f'],
    ['Mervin', 1971, 1, 'm'],
    ['Magdalena', 1979, 1, 'f'],
];


var BasicMRHelper = function () {

    return {
// Diese Funktion filtert das übergebene Array nach dem Geschlecht ('m' | 'f')
// Zum Beispiel: filterGender(persons, 'f')
// Bsp-Rückgabe:
// [ [ 'Maria', 1990, 1, 'f' ],
// [ 'Mareike', 1995, 3, 'f' ],
// [ 'Magdalena', 1979, 1, 'f' ] ]
        filterGender: function (personsArray, gender) {
            return personsArray.filter(c => c[3] === gender);
        },

// Diese Funktion berechnet das Alter jeder Person
// Zum Beispiel: getAge(persons)
// Bsp-Rückgabe:
// [ [ 'Max', 50 ],
// [ 'Maria', 27 ],
// [ 'Meik', 35 ],
// [ 'Mareike', 22 ],
// [ 'Mervin', 46 ],
// [ 'Magdalena', 38 ] ]
        getAge: function (personsArray) {
            return personsArray.map(c => [c[0], ((new Date()).getFullYear() - c[1])]);
        },


// Diese Funktion berechnet den Status (Alter * Statuszahl) jeder Person
// und sortiert das Array (höchster Status zuerst)
// Zum Beispiel: getSortedStatus(persons)
// Bsp-Rückgabe:
// [ [ 'Max', 100 ],
// [ 'Mareike', 96 ],
// [ 'Meik', 70 ],
// [ 'Mervin', 46 ],
// [ 'Magdalena', 38 ],
// [ 'Maria', 27 ] ]
        getSortedStatus: function (personsArray) {
            //    return personsArray.map(c => [c[0], (((new Date()).getFullYear() - c[1]) * c[2])])
            //        .sort((p, c) => p[1] < c[1] ? 1 : (p[1] > c[1] ? -1 : (p[0].toLowerCase() < c[0].toLowerCase() ? -1 : 1)));

            return personsArray.map(person => [person[0], (2017 - person[1]) * person[2]])
                .sort((p1, p2) => {
                    var vergleich = p2[1] - p1[1];
                    // Wenn der selbe Status, dann sortiere nach Namen
                    if (vergleich == 0) {
                        vergleich = p1[0].localeCompare(p2[0], 'de', {sensitivity: "base"});
                    }
                    return vergleich;
                });
        },


// Diese Funktion gibt ein Array mit den Namen aller Personen zurück
// Zum Beispiel: getNames(persons)
// Bsp-Rückgabe:
// [ 'Max', 'Maria', 'Meik', 'Mareike', 'Mervin', 'Magdalena' ]

        getNames: function (personsArray) {
            return personsArray.map(c => c[0]);
        },


// Diese Funktion gibt das addierte Alter aller Personen zurück
// Zum Beispiel: getAggregatedAge(persons)
// Bsp-Rückgabe:
// 228

        getAggregatedAge: function (personsArray) {
            return this.getAge(personsArray).reduce((p, c) => p += c[1], 0);
        },


// Diese Funktion gibt ein Objekt zurück, welches zwei Eigenschaften hat: 'Woman' und 'Men'
// Der Wert entspricht der Anzahl der Frauen und Männer im übergebenen Array
// Zum Beispiel: getAmount(persons)
// Bsp-Rückgabe:
// { Woman: 3, Men: 3 }
        //getAmount: function (personsArray) {
        //    return personsArray
        //        .reduce((p, c) => {
        //            p[c[3]] = (p[c[3]] || 0) + 1;
        //            return p;
        //        }, {})
        //}

        getAmount: function (personsArray) {
            return personsArray.reduce((p, c) => {
                if (c[3] === 'f') p.Woman++; else p.Men++;
                return p;
            }, {Woman: 0, Men: 0})
        }
    }
}();

// console.log(BasicMRHelper.filterGender(personsHelper, 'f'));
// console.log(BasicMRHelper.getAge(personsHelper));
// console.log(BasicMRHelper.getSortedStatus(personsHelper));
// console.log(BasicMRHelper.getNames(personsHelper));
// console.log(BasicMRHelper.getAggregatedAge(personsHelper));
// console.log(BasicMRHelper.getAmount(personsHelper));
