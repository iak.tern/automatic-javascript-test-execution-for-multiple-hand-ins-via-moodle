var HOFSpyWrapper = function () {
    return {
        registerHOFSpies: function (obj) {
            spyOn(obj, "map").and.callThrough();
            spyOn(obj, "forEach").and.callThrough();
            spyOn(obj, "reduce").and.callThrough();
            spyOn(obj, "sort").and.callThrough();
            spyOn(obj, "filter").and.callThrough();
            spyOn(obj, "find").and.callThrough();
            spyOn(obj, "some").and.callThrough();
            spyOn(obj, "every").and.callThrough();
        },

        hofUsed: function (obj) {
            return obj.map.calls.count() > 0
                || obj.forEach.calls.count() > 0
                || obj.reduce.calls.count() > 0
                || obj.sort.calls.count() > 0
                || obj.filter.calls.count() > 0
                || obj.find.calls.count() > 0
                || obj.some.calls.count() > 0
                || obj.every.calls.count() > 0;
        }
    }
}();