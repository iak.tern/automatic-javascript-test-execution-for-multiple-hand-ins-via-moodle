# Moodle-Karma-Test-execution

Mit diesem Projekt werden JavaScript-Abgaben, die in Moodle von Studenten hochgeladen wurden, automatisiert getestet und sowohl eine Bewertungsdatei als auch eine Feedback-Datei zum Hochladen auf Moodle generiert.

## Feature
- Entpacken aller Abgaben in einen Unterordner
- Test-Suiten/-Cases in Jasmine Schreiben
- Erstellte Testszenarien für jede Abgabe durchführen
- Test auf Plagiate
- Erstellen einer Gesamt-Bewertungsliste (erreichte Punkte) zum komfortablen Hochladen zu Moodle
- Erstellen einzelner Feedback-Dateien für jeden Studenten und einer gesamten `feedback.zip` zum Hochladen zu Moodle

## Installation

- [Node.js Version 8 oder höher mit nvm installieren](https://github.com/creationix/nvm/blob/master/README.md#installation)
- [Duometer für Plagiate](https://github.com/pmandera/duometer#debian-ubuntu) (erfordert eine Java-Runtime, z.B. OpenJDK)
- Git Repo klonen

## Konfiguration (kurz)
- einen entsprechenden Unterordner (Semester und Hausaufgabe) im Verzeichnis `Abgaben-Und-Evaluationen` erstellen = das ist der *Testordner*
- Moodle-Abgaben-Datei (Zip-Datei) in den *Testordner* laden mit alle eingereichten JS Lösungen
- Moodle-Bewertungstabelle (CSV-Datei) in den *Testordner* laden
- im *Testordner* einen Unterordner für Testfälle mit dem Namen `Test-Specifications` anlegen (und eventuelle Hilfsklassen in `Test-Specifications/Helper-Classes`)
- Konfig-Datei anlegen/anpassen:
    - Punkte pro Aufgabe in `Test-Specifications/pointsSpec.json` hinterlegen


## Ausführung
`./runTests.sh [-z Abgaben-Moodle.zip] [-o Ergebnis-Ordner] [-b Bewertungstabelle-Moodle.csv] Test-Ordner`
- `-z`: Angabe der von Moodle geladenen Zip-Datei mit allen Abgaben
    - wenn keine Angabe, dann wird automatische die einzige Zip-Datei im Testordner genommen
    - wenn keine Angabe und keine Zip-Datei vorhanden, dann wird vermutet, dass alle Abgaben bereits im Unterordner `Abgaben` entpackt wurden
- `-o`: Angabe des Ordnernamens für die Ergebnisse, Default: "Ergebnisse"
- `-b`: Angabe der heruntergeladenen Moodle-CSV-Bewertungsdatei
    - wenn keine Angabe, dann wird automatische die einzige CSV-Datei im Testordner genommen
- `Test-Ordner`: relativer Pfad von der Datei runTests.sh ausgehend

Beispiel:
`./runTests.sh Abgaben-Und-Evaluationen/17-18-WS/06-HA-Higher-Order-Functions/`


## Vorbereitung: Schreiben von Testfällen bevor kompletter automatiserter Durchlauf stattfindet

**Problem**: vollständiger automatisierter Durchlauf aller Abgaben dauert meist lange

**Lösung zum kurzen Testen**

- mit nur wenigen Abgaben testen, d.h:
    - Ordner "Abgaben" anlegen
    - dort ein paar Ordner für Studenten einfügen und dessen JS-Datei aus Moodle hineinladen
    - Wichtig: nicht die komplette Abgaben-Datei (.zip) herunterladen
    - Testfälle erneuern/schreiben
- für Test einfach normale Ausführung mit `./runTests.sh Test-Ordner`

## Ergebnisse
This program outputs to the *Ergebnisse*-folder
* a csv-file which can be uploaded to moodle/Moodle to directly import the results (*Bewertungen...csv*)
* a zip-file which contains the junit-xml-files named in a way which can be uploaded to moodle so that every student can get individual feedback (*feedback.zip*)

## Konfiguration (lang)

- Please note that in order to sucessfully get the necessary files from Moodle, please follow the first steps of [this guide](https://epress.earlham.edu/tsardocs/2013/11/07/workflow-for-offline-grading-of-written-assignments/)

Testfälle im Ordner `Testordner/Test-Specifications`:
- mehrere Testklassen
- The name in the  `describe()`-function has to end with an colon, since junit-files concat the name of the class with the name of the test-cases thus making it impossible to aggregate over them.

`Testordner/Test-Specifications/pointsSpec.json`:
- Format: see/copy last file
- The name of the exercise in this json has to be exactly the same as the name of the test-class put into its `describe()`-function.
- *Achtung*: es wurde irgendwann die Möglichkeit zum Abzug von Punkten hinzugefügt. Deswegen ist das Testsystem nicht mehr unbedingt mit älteren Archiven/Abgaben ausführbar, weil sich der Aufbau der Datei geändert hat.


`./Evaluation-Framework/karmaTest.conf.js`: 
- Konfiguration von Karma, z.B. anderer Browser festlegen (aktuell: JSDom)
- Karma wurde eigentlich nur verwendet, um einen guten Reporter zu nutzen; kann ersetzt werden




