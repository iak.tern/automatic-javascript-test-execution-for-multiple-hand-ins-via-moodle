#!/bin/bash


frameworkDir=$(pwd)/Evaluation-Framework
abgabenZip=        		# Heruntergeladene Abgaben aus ISIS
abgabenDir=Abgaben 		# Ordner für die entpackten Abgaben
outputDir=Ergebnisse	# Ergebnis-Ordner, Default-Ordner "output"
bewertungCsv=      		# Heruntergeladene Bewertungsdatei aus ISIS
testSpecDir=Test-Specifications # Ordner in dem sich die Testspezifikationen befinden
pointsFile=pointsSpec.json # Datei in dem die Punkte für die Aufgaben vergeben sind, unter testSpecDir/

while getopts z: opt
do
    case "$opt" in
      z)  abgabenZip="$OPTARG";;
      o)  outputDir="$OPTARG";;
      b)  bewertungCsv="$OPTARG";;
      \?)		# unknown flag
      	  echo >&2 \
	  "usage: $0 [-z ISIS-Abgabe.zip] [-o output-directory] [-b ISIS-Bewertungstabelle.csv] Test-Ordner"
	  exit 1
        ;;
    esac
done
# Weise Test-Ordner zu var $1 zu
shift $(expr $OPTIND - 1)

# Wenn kein Test-Ordner angegeben, beende
if [[ -z "$1" ]]
then
    echo -e "usage: $0 [-z ISIS-Abgabe.zip] [-o output-directory] [-b ISIS-Bewertungstabelle.csv] Test-Ordner"
	exit 1
fi

testDir=$(pwd)/"$1"
if [[ ! -d "$testDir" ]]
then
	echo -e "Angegebener Test-Ordner existiert nicht: $testDir"
	exit 1
fi

#if [ $bewertungCsv == '' ]; then		
#	echo >&2 \
#	"ERROR: no moodle-csv-file specified"
#	exit 1;;
#fi


# Installiere notwendige Node.js Libraries
(
cd "$frameworkDir"
if [ ! -d "node_modules" ]; then		
	npm install
	echo -e "\nNode.js Module installiert\n\n"
fi
)



(
cd "$testDir"

#Wenn abgabenZip bereits angegeben wurde, teste ob vorhanden
if [[ -n "$abgabenZip" ]]
then
	if [[ ! -f "$abgabenZip" ]]
	then
		echo -e "Angegebene $abgabenZip Datei existiert nicht im Ordner $testDir. Bitte korrekt angeben oder Angabe weglassen für automatisches Finden."
		exit 1
	fi
# Keine Datei angegeben
else 
	# Finde automatisch Abgaben-Zip-Datei
	# es darf nur max eine Zip-Datei im Ordner existieren
	# es braucht keine Zip-Datei vorhanden sein, dann wird später nichts entpackt
	if [[ $(ls *.zip 2> /dev/null | wc -w) -le 1 ]]
	then
		abgabenZip=$(ls *.zip 2> /dev/null)
	else
		echo -e "Es darf nur max eine Zip-Datei (die gepackten Abgaben aus ISIS) im Test-Ordner existieren"
		ls *.zip -l1
		exit 1
	fi
fi

# Entpacke alle Abgaben in den Abgaben-Ordner
if [[ -f "$abgabenZip" ]]
then
	echo -e "\nEntpacke alle Abgaben von $abgabenZip in den Ordner /$abgabenDir"
    unzip "$abgabenZip" -d "$abgabenDir"/
    echo -e "\nAbgaben entpackt\n\n"
    cp "$abgabenZip" "$abgabenDir"
fi


# Wenn Ausgabe-Ordner noch nicht existiert, dann erstelle
if [[ ! -d "$outputDir" ]]; then
	mkdir "$outputDir"
fi


# Erstelle Liste mit allen abgegebenen JS-Dateien
find "$abgabenDir"/**/*.js -type f > "$outputDir"/allJs.txt

echo -e "\nStarte Tests..."
# Führe alle Tests aus für jede Abgabe
# Ergebnis: Feedback-Dateien unter $outputDir
"$frameworkDir"/performAllTests.sh "$frameworkDir" "$testDir" "$abgabenDir" "$outputDir"
echo -e "\nAlle Tests beendet\n"




#Wenn bewertungCsv bereits angegeben wurde, teste ob vorhanden
if [[ -n "$bewertungCsv" ]]
then
	if [[ ! -f "$bewertungCsv" ]]
	then
		echo -e "Angegebene $bewertungCsv Datei existiert nicht im Ordner $testDir. Bitte korrekt angeben oder Angabe weglassen für automatisches Finden."
		exit 1
	fi
# Keine Datei angegeben
else 
	# Finde automatisch Bewertungstabelle
	# es darf nur max eine CSV-Datei im Ordner existieren
	if [[ $(ls *.csv 2> /dev/null | wc -w) -eq 1 ]]
	then
		bewertungCsv=$(ls *.csv 2> /dev/null)
	else
		echo -e "Bewertungstabelle nicht gefunden. Bepunktung kann nicht in ISIS-Tabelle gespeichert werden."
	fi
fi

if [[ -f "$bewertungCsv" ]]
then
	# Erstelle detaillierte Resultat-Tabelle und schreibe Ergebnisse in Bewertungstabelle
	node "$frameworkDir/index.js" "$testDir" "$bewertungCsv" "$testSpecDir/$pointsFile" "$abgabenDir" "$outputDir"
	echo -e "\nErgebnis und Bewertungsdateien geschrieben"
fi


# Test auf Plagiate
hash duometer 2>/dev/null || { echo >&2 "duometer is required but not installed. You can download it from https://github.com/pmandera/duometer"; exit 1; }
duometer -i "$outputDir"/allJs.txt -o "$outputDir"/plagiate.csv -t 0.1
echo -e "\nAuf Plagiate überprüft\n"
)
