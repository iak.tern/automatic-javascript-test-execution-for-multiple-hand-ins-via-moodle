#!/bin/bash

frameworkDir=$1
testDir=$2
abgabenDir=$3
outputDir=$4
feedbackDir="$outputDir"/feedback
karmaServerPort=9876


cd "$testDir"

# Wenn Feedback-Ordner noch nicht existiert, dann erstelle
if [ ! -d "$feedbackDir" ]; then
	mkdir "$feedbackDir"
fi

cd "$abgabenDir"

# Für jede Abgabe
for DIR in */; do
    # Wieso ist die folgene IF-Bedingung drin? Wir sind doch im Ordner "hand-ins"
	# if [[ $DIR == 'config/' || $DIR == 'node_modules/' || $DIR == 'test-classes/' || $DIR == 'feedback/' ]]; then
	#	continue;
	# fi

	
	(
		
		# Kopiere Karma Konfiguration
		cd "$DIR"
		if [ ! -f "karmaTest.conf.js" ]; then		
			ln -s "$frameworkDir"/karmaTest.conf.js
		fi


		echo -e "\nCurrently viewed directory $DIR\n";
                
        # Führe Karme-Test aus
        # Schreibe Stdout und Stderr in Datei
		node "$frameworkDir"/node_modules/karma/bin/karma start karmaTest.conf.js  &> tmpTestOutput.txt
		
		echo -e "Test ausgeführt"

     
                
        # Kopiere Resultat in Feedback-Ordner
		resultName=$(basename "$PWD")result.html
		cp result.html "$testDir"/"$feedbackDir"/"$resultName"
		
	)

done



# Erstelle eine Zip-Datei von allen Feedbacks
zip -jr "$testDir"/"$outputDir"/feedback.zip "$testDir"/"$feedbackDir" 



