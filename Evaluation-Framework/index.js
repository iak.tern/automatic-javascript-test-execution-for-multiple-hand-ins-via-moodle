'use strict';

const Parse = require('papaparse'),
    fs_prom = require('fs-promise'),
    path = require('path'),
    parseXml = require('batch-junit-parser').parseXml,
    json2csv = require('json2csv');

//require('intl');

const exemptDirs = ['test-classes', 'config', 'node_modules', 'feedback'];


const basePath = process.argv[2],
	fileInput = process.argv[3],  // input csv-file from isis
    tasksToTest = process.argv[4],  // json with the point-values of the sub-exercises
    inputDir = process.argv[5],     // Ordner mit allen getesteten Abgaben
    outputDir = process.argv[6],    // Ordner mit allen Testergebnissen
    detailedResultFile = "detailedResults.csv"; // Name der Ergebnis-Datei
    



const outputPath = path.isAbsolute(outputDir) ? outputDir : path.join(basePath, outputDir);



let createDetailedCsv = (jsonInput) => {
    let jsonTransform = jsonInput.map(r => {
        let out = r.reduce((prev, curr) => {

            if (curr.exercise in prev) {
                prev[curr.exercise] = prev[curr.exercise] && curr.success;
            } else {
                prev[curr.exercise] = curr.success;
            }

            return prev;
        }, {});
        out['name'] = r.name;

        return out;
    });
    let keys = Object.keys(jsonTransform[0]).sort((prev, curr) => {
        if (prev === 'name') return -1;
        if (curr === 'name') return 1;

        if (parseInt(prev.replace(/\D+/g, '')) < parseInt(curr.replace(/\D+/g, ''))) {
            return -1;
        } else {
            return 1;
        }
    });


    return buildCsv(jsonTransform, keys);
};

let buildCsv = (data, keys) => {
    return new Promise((resolve, reject) => {
        try {
            resolve(json2csv({data: data, fields: keys}))
        } catch (err) {
            reject(err)
        }

    });
};


let aggregateResults = (results) => {
    return new Promise((resolve, reject) => {

        let resultsOut = results.map(r => {
            let out = r.reduce((prev, curr) => {

                let currExName = curr.exercise.split(':')[0];

                if (currExName in prev) {
                    prev[currExName] = prev[currExName] && curr.success;
                } else {
                    prev[currExName] = curr.success;
                }

                return prev;
            }, {});
            out['name'] = r.name;

            return out;
        });

        resolve(resultsOut);
    })
};

let mergeResultsWithCSV = (loadedData) => {
    return new Promise((resolve, reject) => {
        let results = loadedData[0],
            moodleCsv = loadedData[1],
            specs = loadedData[2];

        let weightedResults = calculateResultPoints(results, specs);


        let csvIDs = moodleCsv.data.reduce((prev, curr) => {
            prev.push(curr.ID);
            return prev;
        }, []);


        weightedResults.forEach(curr => {
            let csvID = csvIDs.find(f => f.replace(/\D+/g, '') === curr.name.replace(/\D+/g, ''));

            let maxPoints = Object.keys(specs)
                .map(elem => specs[elem])
                .reduce((p, c) => c.malus ? p : p + c.points, 0);

            let currMoodle = moodleCsv.data.find(c => c.ID === csvID);
            currMoodle.Bewertung = ((curr.points * parseInt(currMoodle.Bestwertung)) / maxPoints).toString().replace('.', ',');
        });

        moodleCsv.data.forEach(row => {
            if (row.Bewertung === '') {
                row.Bewertung = 0;
            }
        });

        resolve(moodleCsv);

    })
};


let calculateResultPoints = (results, specs) => {
    return results.map((currStud) => {
        let out = {};

        out['name'] = currStud.name;

        out['points'] = Object.keys(currStud)
            .filter(c => c !== 'name')
            .map(elem => {
                let pointValue;

                if (specs[elem].malus) {
                    pointValue = (!currStud[elem]) * specs[elem].points * -1;
                } else {
                    pointValue = currStud[elem] * specs[elem].points;
                }

                return pointValue
            })
            .reduce((p, c) => p + c, 0);

        if (out['points'] < 0) {
            out['points'] = 0
        }

        return out;
    })
};

let parseJson = (json) => {
    return new Promise((resolve, reject) => {
        resolve(JSON.parse(json));
    })
};


// Erstelle detaillierte Bewertungsdatei
let results = parseXml(path.join(basePath, inputDir), exemptDirs);
results.then(createDetailedCsv)
    .then(content => fs_prom.writeFile(path.join(outputPath, detailedResultFile), content))
    .catch(console.error);

// Lade Bewertungstabelle
let inputCsv = fs_prom.readFile(fileInput, {encoding: 'utf8'})
    .then(data => new Promise((resolve, reject) => resolve(data.replace(/^\uFEFF/, ''))));
let loadedCsv = inputCsv.then(data => new Promise((resolve, reject) => resolve(Parse.parse(data, {header: true}))));

// Datei mit den Punkten pro Aufgabe
let specJson = fs_prom.readFile(tasksToTest, {encoding: 'utf8'}).then(parseJson);



// Schreibe Resultate in Bewertungstabelle.csv
Promise.all([results.then(aggregateResults), loadedCsv, specJson])
    .then(mergeResultsWithCSV)
    .then(csv => buildCsv(csv.data, csv.header))
    .then(content => {
		fs_prom.writeFile(path.join(outputPath, fileInput), content)
		// Überschreibe Ursprungsdatei
		fs_prom.writeFile(path.join(basePath, fileInput), content)
	})
    .catch(console.error);




